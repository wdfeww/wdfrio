# wdfRio

This addon allows players on server felsong.gg use command /rio and get mythic+ score information in-game

**Prerequisites:**
 1. Make sure you download the full client from https://felsong.gg/en/welcome/play otherwise, this addon might not work properly for you

**Installation:**

 1. download the zip file
 2. extract the content 
 3. navigate inside the **wdfrio-main** where you will find the **wdfRio** folder
 4. Copy the **wdfRio** folder inside your addons folder.
  
**Commands and usage:**

`/rio <player_name>` - shows score of defined name

`/rio #<player_rank>` - shows score of defined rank

`/rio` - shows the score of the target and in case of no target defined shows the score of the player executing the command

**Note:**
The first time you use `/rio` command for a specific player will always return a message that the player was not found(because your local database is empty). But at the same time, your addon will request to get the score of this player. The response will come approx 15 seconds(depending on network traffic) so you can try to use `/rio` for the same player again and see if your database has been updated.

**In case of a false score was provided by any of the players you can block a specific player's name.**

`/rio !<player_name>` - this will block player

`/rio $<player_name>` - this will unblock player

**Config:**

You can change default values inside config.lua to tweak the addon messaging system. (Change it only if you have problems with performance)

`interval` - delay between batch message processing (allowed values 1 - 10)

`batch` - how many messages do you want to process at once (allowed values 1 - 5)

`size` - size of message queue - basically this value says how many:
 1. outgoing messages you allow to store before is sent 
 2. incoming messages you allow to store before they are processed  
 (allowed values 1 - 100)

# ***The score will be continuously updated!***

# ***Keep in mind that you should always logout properly by using the logout button in-game because your local database will get saved only by that***


# ***If you like my work and you wanna support me => https://paypal.me/wdfeww***